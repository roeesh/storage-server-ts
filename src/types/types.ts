/* eslint-disable @typescript-eslint/no-explicit-any */
import { NextFunction, Request, Response } from "express";


export type middleWareFunction = (
    req: Request,
    res: Response,
    next: NextFunction
) => any;

export interface IResponseMessage {
    status: number;
    message: string;
    data: any;
}

export interface IErrorResponse {
    status: number;
    message: string;
    stack?: string;
}