import express from "express";
import log from "@ajar/marker";
import morgan from "morgan";
import cors from "cors";

import saver_controller from "./modules/saver.controller.js";
import { error_handler, not_found, responseWithError, urlNotFoundHandler } from "./middleware/errors.handler.js";

const { PORT, HOST = "localhost" } = process.env;

class ExpressApp {
    app = express();

    private setMiddlewares() {
        this.app.use(cors());
        this.app.use(morgan("dev"));
    }

    private setRoutings() {
        this.app.use("/saver", saver_controller);
    }

    private setErrorHandlers() {
        this.app.use(urlNotFoundHandler);
        this.app.use(responseWithError);
        this.app.use(error_handler);
    }

    private setDefault() {
        this.app.use("*", not_found);
    }

    async start() {
        this.setMiddlewares();
        this.setRoutings();
        this.setErrorHandlers();
        this.setDefault();

        await this.app.listen(Number(PORT), HOST as string);
        log.magenta("api is live on", ` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);
    }
}

const instance = new ExpressApp();
export default instance;
