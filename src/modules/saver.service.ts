/* eslint-disable @typescript-eslint/no-explicit-any */
import { Request } from "express";
import fs from "fs";

// Save payload to file
export function savePayloadToFile(req: Request){
    const fileName = req.headers["db-name"];
    const fileToWrite = fs.createWriteStream(`./${fileName}.sql`);
    req.pipe(fileToWrite);
}