import express, { Request, Response } from "express";
import { savePayloadToFile } from "./saver.service.js";
import raw from "../middleware/route.async.wrapper.js";
import { IResponseMessage } from "../types/types.js";

const router = express.Router();

router.use(express.json());

router.route("/").post(raw(async (req: Request, res: Response) => {
    savePayloadToFile(req);
    const outputResponse: IResponseMessage = {
        status: 200,
        message: "File saved..",
        data: "",
    };
    req.on("end",()=>{
        res.status(outputResponse.status).json(outputResponse);
    });
}));

export default router;
