-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: playlist-crud
-- ------------------------------------------------------
-- Server version	8.0.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `artists`
--

DROP TABLE IF EXISTS `artists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `artists` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `nickname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `about` varchar(255) DEFAULT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `artists`
--

LOCK TABLES `artists` WRITE;
/*!40000 ALTER TABLE `artists` DISABLE KEYS */;
INSERT INTO `artists` VALUES (1,'Itay','Zvulun','tuna','tuna123@gmail.com','israeli rapper','pending','2022-01-21 01:23:20','2022-01-21 01:23:20'),(2,'Ravid','Plotnick','Nechi-Nech','nech89@gmail.com','An israeli rapper.','pending','2022-01-23 23:42:13','2022-01-23 23:42:13'),(3,'Shlomo','Arzi','Shlomo Arzi','shlomke@gmail.com','The top 1 israeli artist.','pending','2022-01-24 13:05:36','2022-01-24 13:05:36'),(4,'Idan','Amedi','Amedi','amedi@gmail.com','An israeli artist and actor.','pending','2022-01-24 13:12:35','2022-01-24 13:12:35'),(5,'Amir','Benayon','Amir','Benayon@gmail.com','An israeli artist.','pending','2022-01-24 13:15:36','2022-01-24 13:15:36'),(7,'Omer','Adam','Omer','omeradam@gmail.com','An worldwide israeli singer','pending','2022-01-24 13:25:18','2022-01-24 13:25:18'),(8,'Itay','Levi','Itay Levi','itaylevi@gmail.com','An israeli mizrahit singer','pending','2022-01-24 13:36:46','2022-01-24 13:36:46'),(9,'Eyal','Golan','Eyal Golan','eyalgolan@gmail.com','An israeli mizrahit singer','pending','2022-01-24 13:48:15','2022-01-24 13:48:15'),(10,'Marshall','Bruce','Eminem','Marshall@gmail.com','An worldwide rapper','pending','2022-01-24 13:52:58','2022-01-24 14:55:51');
/*!40000 ALTER TABLE `artists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entity_status`
--

DROP TABLE IF EXISTS `entity_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `entity_status` (
  `index` int unsigned NOT NULL AUTO_INCREMENT,
  `status` varchar(255) NOT NULL,
  PRIMARY KEY (`index`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entity_status`
--

LOCK TABLES `entity_status` WRITE;
/*!40000 ALTER TABLE `entity_status` DISABLE KEYS */;
INSERT INTO `entity_status` VALUES (1,'accepted'),(2,'pending'),(3,'rejected');
/*!40000 ALTER TABLE `entity_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlist_song`
--

DROP TABLE IF EXISTS `playlist_song`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `playlist_song` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `playlist_id` int unsigned NOT NULL,
  `song_id` int unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `playlist_song_playlist_id_foreign` (`playlist_id`),
  KEY `playlist_song_song_id_foreign` (`song_id`),
  CONSTRAINT `playlist_song_playlist_id_foreign` FOREIGN KEY (`playlist_id`) REFERENCES `playlists` (`id`) ON DELETE CASCADE,
  CONSTRAINT `playlist_song_song_id_foreign` FOREIGN KEY (`song_id`) REFERENCES `songs` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlist_song`
--

LOCK TABLES `playlist_song` WRITE;
/*!40000 ALTER TABLE `playlist_song` DISABLE KEYS */;
INSERT INTO `playlist_song` VALUES (1,1,4),(3,1,1),(5,1,3),(9,5,13),(10,5,14),(11,5,4);
/*!40000 ALTER TABLE `playlist_song` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `playlists`
--

DROP TABLE IF EXISTS `playlists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `playlists` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `user_id` int unsigned NOT NULL,
  `number_of_songs` int DEFAULT NULL,
  `total_time` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `playlists_created_by_foreign` (`user_id`),
  CONSTRAINT `playlists_created_by_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `playlists`
--

LOCK TABLES `playlists` WRITE;
/*!40000 ALTER TABLE `playlists` DISABLE KEYS */;
INSERT INTO `playlists` VALUES (1,'Songs of Roee',1,NULL,NULL,'2022-01-26 07:51:20','2022-01-26 07:51:20'),(2,'Best Rap songs og Roee',1,NULL,NULL,'2022-01-26 07:51:59','2022-01-26 08:11:47'),(5,'Songs of Shani',2,NULL,NULL,'2022-01-26 09:39:28','2022-01-26 09:39:28');
/*!40000 ALTER TABLE `playlists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `songs`
--

DROP TABLE IF EXISTS `songs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `songs` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `time` varchar(255) NOT NULL,
  `artist_id` int unsigned NOT NULL,
  `lyrics` text,
  `status` varchar(255) NOT NULL DEFAULT 'pending',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `songs_owner_foreign` (`artist_id`),
  CONSTRAINT `songs_owner_foreign` FOREIGN KEY (`artist_id`) REFERENCES `artists` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `songs`
--

LOCK TABLES `songs` WRITE;
/*!40000 ALTER TABLE `songs` DISABLE KEYS */;
INSERT INTO `songs` VALUES (1,'Scharchoret','03:00',1,'ho romieo zo hyta fantazia nehederet','pending','2022-01-24 18:04:02','2022-01-24 18:36:12'),(3,'Rock 30','03:30',1,'hayeled ben 30','pending','2022-01-24 18:09:57','2022-01-24 18:09:57'),(4,'Nigmar','04:24',4,'ani yoshev achaiv karov el hashamaim','pending','2022-01-24 18:20:45','2022-01-25 12:17:04'),(13,'tirkod','04:02',3,'tirkod hi amra li tirkod','pending','2022-01-26 09:10:50','2022-01-26 09:10:50'),(14,'yareah','04:24',3,'etmol aya tov','pending','2022-01-26 09:11:22','2022-01-26 09:11:22');
/*!40000 ALTER TABLE `songs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_roles` (
  `index` int unsigned NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL,
  PRIMARY KEY (`index`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'admin'),(2,'moderator'),(3,'simple'),(4,'guest');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `refresh_token` varchar(255) DEFAULT NULL,
  `roles` varchar(255) NOT NULL DEFAULT 'simple',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Roee','Sharon','roee9@gmail.com','$2b$10$BKBRuXX6u5aAW2jtikOteeoJCSynwjAlWmgFsTsuZdZEbIAzMqyi6','eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwicm9sZXMiOiJhZG1pbiIsImlhdCI6MTY0MzE5Mzg3MSwiZXhwIjoxNjQ4Mzc3ODcxfQ.Cj2hPaau2IiIOATMCLYb31Riesgw9xp_Xsj8E6YeJyU','admin','2022-01-25 18:49:40','2022-01-26 10:44:31'),(2,'Shani','Sabag','shani9@gmail.com','$2b$10$wuQCik9NOb87TE39a8tvX.Oq7EiH8B8p8jZEdS3NBTMy3NuQYWQRC','','simple','2022-01-25 18:51:23','2022-01-26 10:04:08'),(3,'Or','Sabag','or1@gmail.com','$2b$10$PNJTwKfsSC6SIGJol3BMk.lY9Tmmd/FUNI4TjkTlgYyDsrtHJBrBq',NULL,'simple','2022-01-25 18:54:40','2022-01-25 18:54:40'),(4,'Guy','Avidan','guy2@gmail.com','$2b$10$otqEr6VEtfHbeuE37Nyx7uXNi.eMn7m.GOJrnrddYXZLZfz2YA7t6',NULL,'simple','2022-01-25 18:55:19','2022-01-25 18:55:19'),(5,'Gal','Barkan','gal2@gmail.com','$2b$10$x6mn/NpIo0HU47paIQYKkOtLgL1SjDPL0UjAfwCGjAYp4f7iN7SmW',NULL,'simple','2022-01-26 07:12:00','2022-01-26 07:12:00'),(6,'Gal','Barkani','gali2@gmail.com','$2b$10$hnhW7fiycTBWcIRoFc7uB.SgeGcE9YudXVb7wHIjyqHW25xvPaAUW',NULL,'simple','2022-01-26 07:18:44','2022-01-26 07:18:44'),(7,'Shai','Yosef','shai3@gmail.com','$2b$10$iPpFqDOcIg4toyuZAg1sQepwMo9/58OWMbu0x/tKtCfYqPNNGFq4e',NULL,'simple','2022-01-26 07:25:38','2022-01-26 07:25:38'),(8,'Omer','Yosef','omer312@gmail.com','$2b$10$octMbjWRhRfqIZgSePtCsOHlDa2hljL3jmjCUA49ZEDTJPIP402x.',NULL,'simple','2022-01-26 07:27:26','2022-01-26 07:27:26'),(9,'Yoni','Shalom','yoni45@gmail.com','$2b$10$pi80xuAf3YBYA/ZuV30nte/KjDpaBO0RFuljb0NIDYr3ZeMbCIplK',NULL,'simple','2022-01-26 07:28:57','2022-01-26 07:28:57'),(10,'Yosi','Cohen','yosi3@gmail.com','$2b$10$YI8XWyNKOvGAxgC1YDO2lu/VpJGLe.S0msdwkAcz3CVH67uJGC9J.',NULL,'simple','2022-01-26 07:30:34','2022-01-26 07:30:34'),(11,'Yos','Cohe','yos3@gmail.com','$2b$10$1Z07nsPPLYCRyYxAFtYvquptSa0uy90P/7zV/P1THiop2B0Qbb7yW',NULL,'simple','2022-01-26 07:32:15','2022-01-26 07:32:15');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-26 16:48:01
